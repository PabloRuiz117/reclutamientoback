﻿using ReclutamientoBack.Data;
using ReclutamientoBack.Helpers;
using ReclutamientoBack.Helpers.Messages;
using ReclutamientoBack.Models;
using ReclutamientoBack.Repository;

namespace ReclutamientoBack.Services
{
    public class HumanService : IHumanService
    {
        private readonly HumanRepository _humanRepository;
        private readonly ILogger<HumanService> _logger;
        public HumanService(ApplicationDbContext _context, ILogger<HumanService> logger)
        {
            _humanRepository = new(_context);
            _logger = logger;

        }

        public async Task<ResponseHelper> CreateHumanAsync(HumanViewModel humanViewModel)
        {
            ResponseHelper response = new();
            try
            {
                var existsHuman = await _humanRepository.ExistsHumanAsync(humanViewModel.Name);

                if (!existsHuman)
                {
                    Human human = new()
                    {
                        Name = humanViewModel.Name,
                        Age = humanViewModel.Age,
                        Gender = humanViewModel.Gender,
                        Height = humanViewModel.Height,
                        Weight = humanViewModel.Weight,
                    };

                    if (await _humanRepository.CreateHumanAsync(human) > 0)
                    {
                        response.IsSuccess = true;
                        response.Message = HumanMessages.SuccessCreate;
                        return response;
                    }
                    else
                        response.Message = HumanMessages.ErrorCreate;
                }
                else
                    response.Message = HumanMessages.AlReadyExistHuman;
            }
            catch (Exception ex)
            {
                response.Message = HumanMessages.ErrorCreate;
                _logger.LogError("Error Message: ", ex.Message);
            }
            return response;
        }

        public async Task<ResponseHelper> DeleteHumanAsync(string humanId)
        {
            ResponseHelper response = new();
            try
            {
                Human human = await _humanRepository.GetHumanByIdAsync(humanId);

                if (human != null)
                {
                    if (await _humanRepository.DeleteHumanAsync(human) > 0)
                    {
                        response.IsSuccess = true;
                        response.Message = HumanMessages.SuccessDelete;
                        return response;
                    }
                    else
                        response.Message = HumanMessages.ErrorDelete;
                }
                else
                    response.Message = HumanMessages.ErrorDelete;
            }
            catch (Exception ex)
            {
                response.Message = HumanMessages.ErrorDelete;
                _logger.LogError("Error Message: ", ex.Message);
            }
            return response;
        }

        public async Task<List<Human>> GetHumansAsync()
        {
            List<Human> humans = new();
            try
            {
                humans = await _humanRepository.GetHumansAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error Message: ", ex.Message);
            }
            return humans;
        }

        public async Task<ResponseHelper> UpdateHumanAsync(string humanId, HumanViewModel humanViewModel)
        {
            ResponseHelper response = new();
            try
            {
                var existsHuman = await _humanRepository.ExistsHumanAsync(humanViewModel.Name);

                if (!existsHuman)
                {
                    Human human = await _humanRepository.GetHumanByIdAsync(humanId);

                    if (human != null)
                    {
                        human.Name = humanViewModel.Name;
                        human.Age = humanViewModel.Age;
                        human.Gender = humanViewModel.Gender;
                        human.Weight = humanViewModel.Weight;
                        human.Height = humanViewModel.Height;


                        if (await _humanRepository.UpdateHumanAsync(human) > 0)
                        {
                            response.IsSuccess = true;
                            response.Message = HumanMessages.SuccessUpdate;
                            return response;
                        }
                        else
                            response.Message = HumanMessages.ErrorUpdate;
                    }
                }
                else
                    response.Message = HumanMessages.AlReadyExistHuman;
            }
            catch (Exception ex)
            {
                response.Message = HumanMessages.ErrorUpdate;
                _logger.LogError("Error Message: ", ex.Message);
            }
            return response;
        }
    }
}
