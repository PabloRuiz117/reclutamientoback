﻿using ReclutamientoBack.Helpers;
using ReclutamientoBack.Models;

namespace ReclutamientoBack.Services
{
    public interface IHumanService
    {
        Task<List<Human>> GetHumansAsync();
        Task<ResponseHelper> CreateHumanAsync(HumanViewModel humanViewModel);
        Task<ResponseHelper> UpdateHumanAsync(string humanId, HumanViewModel humanViewModel);
        Task<ResponseHelper> DeleteHumanAsync(string humanId);
    }
}
