﻿using Microsoft.AspNetCore.Mvc;
using ReclutamientoBack.Models;
using ReclutamientoBack.Services;

namespace ReclutamientoBack.Controllers
{
    [Route("api/humans")]
    [ApiController]
    public class HumansController : ControllerBase
    {
        private readonly IHumanService _humanService;

        public HumansController(IHumanService humanService)
        {
            _humanService = humanService;
        }

        [HttpGet("getHumansAsync")]
        public async Task<List<Human>> GetHumansAsync()
        {
            return await _humanService.GetHumansAsync();
        }

        [HttpPost("createHumanAsync")]
        public async Task<ActionResult> CreateHumanAsync(HumanViewModel humanViewModel)
        {
            var response = await _humanService.CreateHumanAsync(humanViewModel);

            if (!response.IsSuccess) return BadRequest(response);

            return Ok(response);
        }

        [HttpPut("updateHumanAsync/{humanId}")]
        public async Task<ActionResult> UpdateHumanAsync(string humanId, HumanViewModel humanViewModel)
        {
            if (string.IsNullOrEmpty(humanId)) return NotFound(nameof(humanId));

            var response = await _humanService.UpdateHumanAsync(humanId, humanViewModel);

            if (!response.IsSuccess) return BadRequest(response);

            return Ok(response);
        }

        [HttpDelete("deleteHumanAsync/{humanId}")]
        public async Task<ActionResult> DeleteHumanAsync(string humanId)
        {
            if (string.IsNullOrEmpty(humanId)) return NotFound(nameof(humanId));

            var response = await _humanService.DeleteHumanAsync(humanId);

            if (!response.IsSuccess) return BadRequest(response);

            return Ok(response);
        }
    }
}
