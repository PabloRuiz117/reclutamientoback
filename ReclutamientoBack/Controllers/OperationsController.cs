﻿using Microsoft.AspNetCore.Mvc;

namespace ReclutamientoBack.Controllers
{
    [Route("api/operations")]
    [ApiController]
    public class OperationsController : ControllerBase
    {
        [HttpGet("sum/{num1:int}/{num2:int}")]
        public int Sum(int num1, int num2)
        {
            return num1 + num2;
        }

        [HttpGet("subtract/{num1:int}/{num2:int}")]
        public int Subtract(int num1, int num2)
        {
            return num1 - num2;
        }

        [HttpGet("multiply/{num1:int}/{num2:int}")]
        public int Multiply(int num1, int num2)
        {
            return num1 * num2;
        }

        [HttpGet("divide/{num1:int}/{num2:int}")]
        public int Divide(int num1, int num2)
        {
            return num1 / num2;
        }
    }
}
