﻿using Microsoft.EntityFrameworkCore;
using ReclutamientoBack.Models;

namespace ReclutamientoBack.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }

        public virtual DbSet<Human> Humans { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Human>().Property(x => x.Weight).HasColumnType("decimal").HasPrecision(10, 2);
            modelBuilder.Entity<Human>().Property(x => x.Height).HasColumnType("decimal").HasPrecision(10, 2);
        }
    }
}
