﻿using Microsoft.EntityFrameworkCore;
using ReclutamientoBack.Data;
using ReclutamientoBack.Models;

namespace ReclutamientoBack.Repository
{
    public class HumanRepository
    {
        private readonly ApplicationDbContext _context;

        public HumanRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Human>> GetHumansAsync() => await _context.Humans.ToListAsync();

        public async Task<Human> GetHumanByIdAsync(string humanId) => await _context.Humans.FirstOrDefaultAsync(x => x.Id == humanId);

        public async Task<int> CreateHumanAsync(Human human)
        {
            await _context.Humans.AddAsync(human);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateHumanAsync(Human human)
        {
            _context.Humans.Update(human);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> DeleteHumanAsync(Human human)
        {
            _context.Humans.Remove(human);
            return await _context.SaveChangesAsync();
        }

        public async Task<bool> ExistsHumanAsync(string name) => await _context.Humans.AnyAsync(x => x.Name.ToLower() == name.ToLower());
    }
}
