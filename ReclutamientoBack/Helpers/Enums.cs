﻿using System.ComponentModel.DataAnnotations;

namespace ReclutamientoBack.Helpers
{
    public class Enums
    {
        public enum Gender
        {
            [Display(Name = "Masculino")]
            Male,
            [Display(Name = "Femenino")]
            Female
        }
    }
}
