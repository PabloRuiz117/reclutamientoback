﻿namespace ReclutamientoBack.Helpers.Messages
{
    public static class HumanMessages
    {
        public readonly static string SuccessCreate = "El humano ha sido creado con exito.";
        public readonly static string SuccessUpdate = "El humano ha sido editado con exito.";
        public readonly static string SuccessDelete = "El humano ha sido eliminado con exito.";

        public readonly static string ErrorCreate = "Ha ocurrido un error al crear un humano.";
        public readonly static string ErrorUpdate = "Ha ocurrido un error al editar un humano.";
        public readonly static string ErrorDelete = "Ha ocurrido un error al eliminar un humano.";

        public readonly static string AlReadyExistHuman = "Un humano ya existe con este nombre, intente con otro nombre.";
    }
}
